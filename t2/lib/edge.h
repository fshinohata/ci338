#ifndef __PLANE_TRIANGLE_EDGE__
#define __PLANE_TRIANGLE_EDGE__

#include "point.h"
#include "triangle.h"

typedef struct Triangle Triangle;
typedef struct Edge Edge;

struct Edge {
    Point *pointA;
    Point *pointB;

    Triangle *triangleA;
    Triangle *triangleB;
};

void Edge_init(Edge *edge, Point *a, Point *b);
Edge *Edge_new(Point *a, Point *b);
void Edge_free(Edge *edge);
int Edge_compare(Edge *a, Edge *b);

#endif

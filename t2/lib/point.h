#ifndef __PLANE_COORDINATES__
#define __PLANE_COORDINATES__

typedef struct Point Point;

struct Point {
    int id;

    double x;
    double y;
};

void Point_init(Point *c, double x, double y);
Point *Point_new(double x, double y);
void Point_free(Point *c);
int Point_compare(Point *a, Point *b);
int Point_getPosition(Point *a, Point *b, Point *m);
int Point_isToTheRight(int position);
int Point_isCollinear(int position);
int Point_isToTheLeft(int position);

#endif

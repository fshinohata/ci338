#ifndef __REPOSITORY__
#define __REPOSITORY__

typedef struct EdgeRepository EdgeRepository;

struct EdgeRepository {
    Edge **edges;
    int edgesLength __attribute__((aligned(8)));
    int edgesAllocated __attribute__((aligned(8)));
};

EdgeRepository *EdgeRepository_new();
void EdgeRepository_free(EdgeRepository *repository);
void EdgeRepository_add(EdgeRepository *repository, Edge *edge);
Edge *EdgeRepository_search(EdgeRepository *repository, Point *a, Point *b);
Edge *EdgeRepository_searchOrCreate(EdgeRepository *repository, Point *a, Point *b);

#endif
#ifndef __CONSTANTS__
#define __CONSTANTS__

#include <stdio.h>

#define CACHE_LINE_SIZE 64
#define ALIGNMENT CACHE_LINE_SIZE

#define AUDIT_PRINT(format, ...) { fprintf(stderr, "(%s) [%s:%d] " format, __TIME__, __FILE__, __LINE__, ##__VA_ARGS__); fflush(stderr); }

#ifdef _DEBUG_
    #define VERBOSE(format, ...) AUDIT_PRINT(format, ##__VA_ARGS__)
#else
    #define VERBOSE(...)
#endif

#define _reallocIfNecessary(array, length, allocated) \
    if (length == allocated) {\
        register int newSize = allocated << 1;\
        array = alignedRealloc(\
            array,\
            allocated,\
            sizeof(*array),\
            ALIGNMENT,\
            newSize\
        );\
        allocated = newSize;\
    }

#endif
#ifndef __ALIGNED_MEMORY_ALLOCATORS__
#define __ALIGNED_MEMORY_ALLOCATORS__

#include <stddef.h>

void *alignedMalloc(int alignment, size_t size);
void *alignedRealloc(void *ptr, size_t oldSize, size_t memberSize, int alignment, size_t newSize);

#endif
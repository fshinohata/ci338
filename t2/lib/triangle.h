#ifndef __PLANE_TRIANGLE__
#define __PLANE_TRIANGLE__

#include "point.h"
#include "edge.h"

typedef struct Triangle Triangle;
typedef struct Edge Edge;

struct Triangle {
    int id;

    Point *pointA;
    Point *pointB;
    Point *pointC;

    Edge *edgeAB;
    Edge *edgeAC;
    Edge *edgeBC;

    Triangle *oppositeA;
    Triangle *oppositeB;
    Triangle *oppositeC;
};

void Triangle_init(Triangle *t, Point *a, Point *b, Point *c);
Triangle *Triangle_new(Point *a, Point *b, Point *c);
void Triangle_free(Triangle *t);
int Triangle_getId(Triangle *t);

#endif

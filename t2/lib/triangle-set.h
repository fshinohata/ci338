#ifndef __PLANE_TRIANGLE_SET__
#define __PLANE_TRIANGLE_SET__

#include "triangle.h"

typedef struct TriangleSet TriangleSet;
typedef struct TriangleSetFlags TriangleSetFlags;

struct TriangleSetFlags {
    char isClosed:1;
};

struct TriangleSet {
    Triangle **triangles;
    int trianglesLength __attribute__((aligned(8)));
    int trianglesAllocated __attribute__((aligned(8)));
    TriangleSetFlags flags __attribute__((aligned(8)));
} __attribute__((aligned(32)));

void TriangleSet_init(TriangleSet *set);
TriangleSet *TriangleSet_new();
void TriangleSet_freeTriangles(TriangleSet *set);
void TriangleSet_free(TriangleSet *set);
void TriangleSet_add(TriangleSet *set, Triangle *t);
void TriangleSet_close(TriangleSet *set);
int TriangleSet_getPathToPoint(Triangle *initial, Point *p, Triangle ***holder);

#endif

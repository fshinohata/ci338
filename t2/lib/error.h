#ifndef __ERROR_HANDLING__
#define __ERROR_HANDLING__

#include <stdio.h>
#include "const.h"

typedef enum ErrorCode ErrorCode;

enum ErrorCode {
    ErrorCode_OK, /** Nothing happened */
    ErrorCode_InputError, /** Check your input */
    ErrorCode_CodeError, /** Check your code */
    ErrorCode_PossibleInfiniteLoop /** The search is possibly entered an infinite loop */
};

#define ABORT(code, msg, ...) { AUDIT_PRINT(msg, ##__VA_ARGS__); exit(code); }

#endif

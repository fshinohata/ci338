#include <stdio.h>
#include <stdlib.h>
#include "aligned-memory.h"
#include "const.h"
#include "error.h"
#include "point.h"
#include "edge.h"
#include "repository.h"
#include "triangle.h"
#include "triangle-set.h"

static EdgeRepository *edgeRepository = NULL;

static int id = 1;

static void Triangle_createEdgeRepositoryIfNecessary() {
    if (edgeRepository == NULL) {
        edgeRepository = EdgeRepository_new();
    }
}

static void Triangle_setEdgeTriangle(Triangle *t, Edge *e) {
    if (e->triangleA == NULL) {
        e->triangleA = t;
    } else if (e->triangleB == NULL) {
        e->triangleB = t;
    } else {
        ABORT(ErrorCode_InputError,
            "An edge can be part of two triangles only.\n");
    }
}

static void Triangle_setPointsInClockwiseOrder(
    Triangle *t,
    Point *a,
    Point *b,
    Point *c
) {
    Point center = {
        .x = (a->x + b->x + c->x) / 3,
        .y = (a->y + b->y + c->y) / 3
    };

    int position = Point_getPosition(a, b, &center);
    if (Point_isToTheRight(position)) {
        t->pointA = a;
        t->pointB = b;
        t->pointC = c;
    } else {
        t->pointA = a;
        t->pointB = c;
        t->pointC = b;
    }
}

void Triangle_init(Triangle *t, Point *a, Point *b, Point *c) {
    Triangle_createEdgeRepositoryIfNecessary();

    t->id = id++;

    Triangle_setPointsInClockwiseOrder(t, a, b, c);

    t->edgeAB = EdgeRepository_searchOrCreate(edgeRepository, t->pointA, t->pointB);
    t->edgeAC = EdgeRepository_searchOrCreate(edgeRepository, t->pointA, t->pointC);
    t->edgeBC = EdgeRepository_searchOrCreate(edgeRepository, t->pointB, t->pointC);

    Triangle_setEdgeTriangle(t, t->edgeAB);
    Triangle_setEdgeTriangle(t, t->edgeAC);
    Triangle_setEdgeTriangle(t, t->edgeBC);

    t->oppositeA = NULL;
    t->oppositeB = NULL;
    t->oppositeC = NULL;
}

Triangle *Triangle_new(Point *a, Point *b, Point *c) {
    Triangle *t = (Triangle *)alignedMalloc(ALIGNMENT, sizeof(Triangle));
    Triangle_init(t, a, b, c);
    return t;
}

void Triangle_free(Triangle *t) {
    free(t);
}

int Triangle_getId(Triangle *t) {
    if (t == NULL) {
        return 0;
    }
    return t->id;
}

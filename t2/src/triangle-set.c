#include <stdlib.h>
#include "aligned-memory.h"
#include "const.h"
#include "error.h"
#include "point.h"
#include "triangle-set.h"

#define MIN_SIZE 64

void TriangleSet_init(TriangleSet *set) {
    set->triangles = (Triangle **)alignedMalloc(ALIGNMENT, MIN_SIZE * sizeof(Triangle *));
    set->trianglesLength = 0;
    set->trianglesAllocated = MIN_SIZE;
    set->flags.isClosed = 0;
}

TriangleSet *TriangleSet_new() {
    TriangleSet *set = (TriangleSet *)alignedMalloc(ALIGNMENT, sizeof(TriangleSet));
    TriangleSet_init(set);
    return set;
}

void TriangleSet_freeTriangles(TriangleSet *set) {
    for (int i = 0; i < set->trianglesLength; i++) {
        Triangle_free(set->triangles[i]);
    }
}

void TriangleSet_free(TriangleSet *set) {
    free(set->triangles);
    free(set);
}

void TriangleSet_add(TriangleSet *set, Triangle *t) {
    if (set->flags.isClosed) {
        ABORT(ErrorCode_CodeError, "You cannot add triangles to a closed set.");
    }

    set->triangles[set->trianglesLength++] = t;
    t->id = set->trianglesLength;

    _reallocIfNecessary(
        set->triangles,
        set->trianglesLength,
        set->trianglesAllocated
    );
}

// finishes processing the set.
// you cannot add more triangles to a closed set.
void TriangleSet_close(TriangleSet *set) {
    if (set->flags.isClosed) {
        ABORT(ErrorCode_CodeError, "You cannot close");
    }

    set->flags.isClosed = 1;

    // set opposite triangles
    for (int i = 0; i < set->trianglesLength; i++) {
        Triangle *t = set->triangles[i];

        if (t->edgeAB->triangleA == t) {
            t->oppositeC = t->edgeAB->triangleB;
        } else {
            // t->edgeAB->triangleB == t
            t->oppositeC = t->edgeAB->triangleA;
        }

        if (t->edgeAC->triangleA == t) {
            t->oppositeB = t->edgeAC->triangleB;
        } else {
            // t->edgeAC->triangleB == t
            t->oppositeB = t->edgeAC->triangleA;
        }

        if (t->edgeBC->triangleA == t) {
            t->oppositeA = t->edgeBC->triangleB;
        } else {
            // t->edgeBC->triangleB == t
            t->oppositeA = t->edgeBC->triangleA;
        }

        VERBOSE("Opposites of %d: %d %d %d\n",
            t->id,
            Triangle_getId(t->oppositeA),
            Triangle_getId(t->oppositeB),
            Triangle_getId(t->oppositeC)
        );
    }
}

static int TriangleSet_hasVisited(Triangle **path, int pathLength, int id) {
    if (id == 0) return 0;
    for (int i = pathLength - 1; i >= 0; i--) {
        if (path[i]->id == id) return 1;
    }
    return 0;
}

// assumes that the initial triangle is part of the set
// points of triangles are given clockwise
int TriangleSet_getPathToPoint(Triangle *initial, Point *p, Triangle ***holder) {
    Triangle **path = (Triangle **)alignedMalloc(ALIGNMENT, MIN_SIZE * sizeof(Triangle *));
    int pathLength = 0;
    int pathAllocated = MIN_SIZE;

    Triangle *current = initial;

    do {
        path[pathLength++] = current;
        _reallocIfNecessary(path, pathLength, pathAllocated);

        int position = Point_getPosition(current->pointA, current->pointB, p);
        if (Point_isToTheRight(position) || Point_isCollinear(position)) {
            position = Point_getPosition(current->pointB, current->pointC, p);
            if (Point_isToTheRight(position) || Point_isCollinear(position)) {
                position = Point_getPosition(current->pointC, current->pointA, p);
                if (Point_isToTheRight(position) || Point_isCollinear(position)) {
                    break;
                } else {
                    VERBOSE("Going to triangle %d (opposite of point %d)\n", Triangle_getId(current->oppositeB), current->pointB->id);
                    if (!TriangleSet_hasVisited(path, pathLength, Triangle_getId(current->oppositeB))) {
                        current = current->oppositeB;
                    } else {
                        ABORT(ErrorCode_PossibleInfiniteLoop, "Possibly going in circles. Aborting...\n");
                    }
                }
            } else {
                VERBOSE("Going to triangle %d (opposite of point %d)\n", Triangle_getId(current->oppositeA), current->pointA->id);
                if (!TriangleSet_hasVisited(path, pathLength, Triangle_getId(current->oppositeA))) {
                    current = current->oppositeA;
                } else {
                    ABORT(ErrorCode_PossibleInfiniteLoop, "Possibly going in circles. Aborting...\n");
                }
            }
        } else {
            VERBOSE("Going to triangle %d (opposite of point %d)\n", Triangle_getId(current->oppositeC), current->pointC->id);
            if (!TriangleSet_hasVisited(path, pathLength, Triangle_getId(current->oppositeC))) {
                current = current->oppositeC;
            } else {
                ABORT(ErrorCode_PossibleInfiniteLoop, "Possibly going in circles. Aborting...\n");
            }
        }

        if (current == NULL) {
            ABORT(ErrorCode_InputError, "The point (%.20g,%.20g) is outside the triangle set.\n", p->x, p->y);
        }
    } while (1);

    *holder = path;

    return pathLength;
}

#include <stdlib.h>
#include "aligned-memory.h"
#include "const.h"
#include "point.h"
#include "edge.h"
#include "repository.h"

#define MIN_SIZE 64

EdgeRepository *EdgeRepository_new() {
    EdgeRepository *repository = (EdgeRepository *)alignedMalloc(ALIGNMENT, sizeof(EdgeRepository));

    repository->edges = (Edge **)alignedMalloc(ALIGNMENT, MIN_SIZE * sizeof(Edge *));
    repository->edgesLength = 0;
    repository->edgesAllocated = MIN_SIZE;

    return repository;
}

void EdgeRepository_free(EdgeRepository *repository) {
    for (int i = 0; i < repository->edgesLength; i++) {
        Edge_free(repository->edges[i]);
    }

    free(repository->edges);
    free(repository);
}

// insertion sort
void EdgeRepository_add(EdgeRepository *repository, Edge *edge) {
    int i = repository->edgesLength;

    while (i > 0 && Edge_compare(edge, repository->edges[i - 1]) < 0) {
        repository->edges[i] = repository->edges[i - 1];
        i--;
    }

    repository->edges[i] = edge;
    repository->edgesLength++;

    _reallocIfNecessary(
        repository->edges,
        repository->edgesLength,
        repository->edgesAllocated
    );
}

Edge *EdgeRepository_search(EdgeRepository *repository, Point *a, Point *b) {
    Edge edge;
    Edge_init(&edge, a, b);

    int min = 0;
    int max = repository->edgesLength - 1;
    int middle = (min + max) / 2;

    while (middle >= min && middle <= max) {
        int comparison = Edge_compare(repository->edges[middle], &edge);
        if (comparison == 0) {
            return repository->edges[middle];
        } else if (comparison > 0) {
            // middle edge is bigger than what we want
            max = middle - 1;
        } else {
            // middle edge is smaller than what we want
            min = middle + 1;
        }
        middle = (min + max) / 2;
    }

    return NULL;
}

Edge *EdgeRepository_searchOrCreate(EdgeRepository *repository, Point *a, Point *b) {
    Edge *search = EdgeRepository_search(repository, a, b);

    if (search != NULL) {
        VERBOSE("Searching for edge (%d, %d) returned (%d, %d)\n", a->id, b->id, search->pointA->id, search->pointB->id);
        return search;
    }

    Edge *new = Edge_new(a, b);
    EdgeRepository_add(repository, new);

    return new;
}

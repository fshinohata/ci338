#include "double.h"

int Double_compare(double a, double b) {
    return (
        Double_isEqual(a, b) ? 0 :
        a < b ? -1 :
        1
    );
}

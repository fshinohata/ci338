#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "aligned-memory.h"
#include "const.h"
#include "point.h"
#include "triangle.h"
#include "triangle-set.h"

int main(void) {
    VERBOSE("Starting...\n");

    Point target;
    assert(scanf("%lf %lf", &target.x, &target.y) == 2);
    VERBOSE("Target point: ");
    printf("%.20g %.20g\n", target.x, target.y);

    int numberOfPoints;
    assert(scanf("%d", &numberOfPoints) == 1);
    VERBOSE("Number of points to read: ");
    printf("%d\n", numberOfPoints);

    Point *points = (Point *)alignedMalloc(ALIGNMENT, numberOfPoints * sizeof(Point));
    double x;
    double y;

    for (int i = 0; i < numberOfPoints; i++) {
        assert(scanf("%lf %lf", &x, &y) == 2);
        VERBOSE("Read point: ");
        printf("%.20g %.20g\n", x, y);
        Point_init(&points[i], x, y);
    }

    int numberOfTriangles;
    assert(scanf("%d", &numberOfTriangles) == 1);
    VERBOSE("Number of triangles to read: ");
    printf("%d\n", numberOfTriangles);

    TriangleSet *triangleSet = TriangleSet_new();
    int idA;
    int idB;
    int idC;

    for (int i = 0; i < numberOfTriangles; i++) {
        assert(scanf("%d %d %d", &idA, &idB, &idC) == 3);
        Point *a = &points[idA - 1];
        Point *b = &points[idB - 1];
        Point *c = &points[idC - 1];
        Triangle *t = Triangle_new(a, b, c);
        TriangleSet_add(triangleSet, t);
        VERBOSE("Read triangle (%d,%d,%d)\n", idA, idB, idC);
    }

    TriangleSet_close(triangleSet);
    VERBOSE("Closed triangle set\n");

    VERBOSE("Final triangle set:\n");
    for (int i = 0; i < triangleSet->trianglesLength; i++) {
        Triangle *t = triangleSet->triangles[i];
        printf(
            "%d %d %d %d %d %d\n",
            t->pointA->id,
            t->pointB->id,
            t->pointC->id,
            Triangle_getId(t->oppositeA),
            Triangle_getId(t->oppositeB),
            Triangle_getId(t->oppositeC)
        );
    }

    Triangle **path;
    int pathLength = TriangleSet_getPathToPoint(triangleSet->triangles[0], &target, &path);

    for (int i = 0; i < pathLength; i++) {
        printf("%d ", path[i]->id);
    }
    puts("");

    TriangleSet_freeTriangles(triangleSet);
    TriangleSet_free(triangleSet);

    free(path);
    free(points);
    VERBOSE("Finishing program...\n");

    return 0;
}

#include <stdlib.h>
#include "aligned-memory.h"
#include "const.h"
#include "double.h"
#include "point.h"
#include "edge.h"

void Edge_init(Edge *edge, Point *a, Point *b) {
    Point *first;
    Point *second;

    int pointComparison = Point_compare(a, b);

    if (pointComparison <= 0) {
        first = a;
        second = b;
    } else {
        first = b;
        second = a;
    }

    edge->pointA = first;
    edge->pointB = second;
    edge->triangleA = NULL;
    edge->triangleB = NULL;
}

Edge *Edge_new(Point *a, Point *b) {
    Edge *edge = (Edge *)alignedMalloc(ALIGNMENT, sizeof(Edge));
    Edge_init(edge, a, b);
    return edge;
}

void Edge_free(Edge *edge) {
    free(edge);
}

int Edge_compare(Edge *a, Edge *b) {
    int pointAComparison = Point_compare(a->pointA, b->pointA);

    if (pointAComparison == 0) {
        return Point_compare(a->pointB, b->pointB);
    }

    return pointAComparison;
}
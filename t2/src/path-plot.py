#!/usr/bin/python3.7
import numpy as np
import matplotlib.pyplot as plt
import sys

points = []
triangles = []

class Point:
    id = 1

    def __init__(self, x, y):
        self.id = Point.id
        Point.id += 1
        self.x = float(x)
        self.y = float(y)

    @staticmethod
    def fromString(string: str):
        arr = string.split(' ')
        return Point(arr[0], arr[1])

class Triangle:
    id = 1

    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.id = Triangle.id
        Triangle.id += 1
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.center = Point((p1.x + p2.x + p3.x) / 3, (p1.y + p2.y + p3.y) / 3)

    @staticmethod
    def fromString(string: str):
        arr = string.split(' ')
        return Triangle(points[int(arr[0]) - 1], points[int(arr[1]) - 1], points[int(arr[2]) - 1])

targetPoint = Point.fromString(sys.stdin.readline())
numberOfPoints = int(sys.stdin.readline())

for i in range(numberOfPoints):
    points.append(Point.fromString(sys.stdin.readline()))

numberOfTriangles = int(sys.stdin.readline())

for i in range(numberOfTriangles):
    triangles.append(Triangle.fromString(sys.stdin.readline()))

path = sys.stdin.readline().strip().split(' ')
plt.figure(figsize=(10,10))
plt.axis('equal')

for i in range(len(points)):
    point = points[i]
    plt.text(point.x, point.y, i+1, horizontalalignment='center', verticalalignment='center')

for i in range(len(triangles)):
    t = triangles[i]
    x = [t.p1.x, t.p2.x, t.p3.x]
    y = [t.p1.y, t.p2.y, t.p3.y]
    plt.fill(x, y)
    plt.text(t.center.x, t.center.y, 'T{}'.format(t.id), horizontalalignment='center', verticalalignment='center')

for i in range(len(path) - 1):
    t1 = triangles[int(path[i]) - 1]
    t2 = triangles[int(path[i + 1]) - 1]
    delta = Point(t2.center.x - t1.center.x - 0.3, t2.center.y - t1.center.y)
    plt.arrow(t1.center.x, t1.center.y - 0.4, delta.x, delta.y, head_width=0.3, head_length=0.3)

plt.show()

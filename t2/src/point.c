#include <stdlib.h>
#include "aligned-memory.h"
#include "const.h"
#include "double.h"
#include "point.h"

int id = 1;

void Point_init(Point *c, double x, double y) {
    c->id = id++;
    c->x = x;
    c->y = y;
}

Point *Point_new(double x, double y) {
    Point *c = (Point *)alignedMalloc(ALIGNMENT, sizeof(Point));
    Point_init(c, x, y);
    return c;
}

void Point_free(Point *c) {
    free(c);
}

int Point_compare(Point *a, Point *b) {
    int xComparison = Double_compare(a->x, b->x);

    if (xComparison == 0) {
        return Double_compare(a->y, b->y);
    }

    return xComparison;
}

// calculates position os m in relation to vector ab
// values:
// 0: m is collinear to ab
// 1: m is to the left of ab
// -1: m is to the right os ab
int Point_getPosition(Point *a, Point *b, Point *m) {
    // cross product of ab and am
    double crossProduct = (b->x - a->x) * (m->y - a->y) - (b->y - a->y) * (m->x - a->x);

    if (Double_isEqual(crossProduct, 0.0)) {
        return 0;
    }

    return crossProduct > 0 ? 1 : -1;
}

int Point_isToTheRight(int position) {
    return position < 0;
}

int Point_isCollinear(int position) {
    return position == 0;
}

int Point_isToTheLeft(int position) {
    return position > 0;
}

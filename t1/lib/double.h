#ifndef __DOUBLE_MATH__
#define __DOUBLE_MATH__

#define DOUBLE_EPSILON 0.00000000001

#define Double_abs(a) ((a) < 0.0 ? -(a) : (a))
#define Double_isEqual(a, b) (Double_abs((a) - (b)) <= DOUBLE_EPSILON ? 1 : 0)

#endif
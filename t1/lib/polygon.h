#ifndef __POLYGON__
#define __POLYGON__

/* STRUCTS / ENUMS / TYPEDEFS */
typedef struct IntersectionSet IntersectionSet;
typedef struct Intersection Intersection;
typedef struct PolygonSet PolygonSet;
typedef struct Polygon Polygon;
typedef struct PolygonFlags PolygonFlags;
typedef struct Edge Edge;
typedef struct Point Point;

enum Error {
    OK,
    Error_UnsupportedPolygonOrder
};

enum PolygonType {
    PolygonType_Unknown,
    PolygonType_SimpleConvex,
    PolygonType_SimpleConcave,
    PolygonType_NonSimple
};
extern const char *PolygonTypeStrings[];

enum PolygonPointOrder {
    PolygonPointOrder_ClockWise,
    PolygonPointOrder_CounterClockWise
};
extern const char *PolygonPointOrderStrings[];

struct IntersectionSet {
    Intersection **intersections;
    int intersectionsLength __attribute__((aligned(8)));
    int intersectionsAllocated __attribute__((aligned(8)));
} __attribute__((aligned(32)));

struct Intersection {
    double x;
    double y;
    Edge *edgeA;
    Edge *edgeB;
} __attribute__((aligned(32)));

struct PolygonSet {
    Polygon **polygons;
    int polygonsLength __attribute__((aligned(8)));
    int polygonsAllocated __attribute__((aligned(8)));
    int numberOfPoints __attribute__((aligned(8)));
    int numberOfEdges __attribute__((aligned(8)));
} __attribute__((aligned(64)));

struct PolygonFlags {
    char hasPositiveCrossProduct:1;
    char hasNegativeCrossProduct:1;
    char hasSelfIntersection:1;
    char isClosed:1;
};

struct Polygon {
    int id __attribute__((aligned(8)));
    Point *points;
    int pointsLength __attribute__((aligned(8)));
    int pointsAllocated __attribute__((aligned(8)));
    Edge *edges;
    int edgesLength __attribute__((aligned(8)));
    int edgesAllocated __attribute__((aligned(8)));
    enum PolygonType type __attribute__((aligned(8)));
    enum PolygonPointOrder order __attribute__((aligned(8)));
    PolygonFlags flags __attribute__((aligned(8)));
} __attribute__((aligned(128)));

struct Edge {
    Point *head;
    Point *tail;
    Point *leftMost;
    Point *rightMost;
    Point *topMost;
    Point *bottomMost;
    Polygon *polygon;
    double slope;
    double y0;
} __attribute__((aligned(64)));

struct Point {
    int index __attribute__((aligned(8)));
    double x;
    double y;
    double angle;
    Polygon *polygon;
    Edge *head;
    Edge *tail;
} __attribute__((aligned(32)));

/* METHODS */
/* INTERSECTION SET */
void IntersectionSet_freeIntersections(IntersectionSet *set);
void IntersectionSet_free(IntersectionSet *set);

/* POLYGON SET */
PolygonSet *PolygonSet_new();
void PolygonSet_free(PolygonSet *set);
void PolygonSet_freePolygons(PolygonSet *set);
void PolygonSet_addPolygon(PolygonSet *set, Polygon *p);
IntersectionSet *PolygonSet_getIntersections(PolygonSet *set);

/* POLYGON */
Polygon *Polygon_new(enum PolygonPointOrder pointOrder);
void Polygon_free(Polygon *p);

int Polygon_isSimple(Polygon *p);
int Polygon_isConvex(Polygon *p);
void Polygon_close(Polygon *p);

void Polygon_addPoint(Polygon *p, double x, double y);

Polygon *Polygon_readFile(FILE *input, enum PolygonPointOrder order);
void Polygon_print(FILE *output, Polygon *polygon);

#endif
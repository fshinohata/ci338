#ifndef __GENERIC_LIST__
#define __GENERIC_LIST__

typedef struct CircularList CircularList;
typedef struct CircularListItem CircularListItem;

struct CircularList {
    CircularListItem *head;
    int size __attribute__((aligned(8)));
};

struct CircularListItem {
    CircularListItem *previous;
    CircularListItem *next;
    void *data;
};

CircularList *CircularList_new();
void CircularList_free(CircularList *l);
void *CircularList_pop(CircularList *list);
void CircularList_push(CircularList *list, void *data);
void *CircularList_tryRemove(CircularList *list, void *data);
void CircularList_forEach(CircularList *list, void (*consume)(void *data));

#endif
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# USO:
# Para desenhar um arquivo de entrada do Trabalho 1, faça:
# $ python drawtrab1.py < arquivo


# tutorial completo em
# http://matplotlib.sourceforge.net/users/pyplot_tutorial.html
import sys
import matplotlib
from matplotlib.pyplot import figure, show, fill, text, draw
import math

def le_poligono(p = 0):
    global abscissas
    global ordenadas
    global maxx
    global maxy
    global minx
    global miny

    ax = []
    ay = []
    n=int(sys.stdin.readline()) #
    for i in range(n):
        a=(sys.stdin.readline().split())
        x = int(a[0])
        y = int(a[1])
        ax.append(x)
        ay.append(y)
        if x > maxx:
            maxx = x
        if x < minx:
            minx = x
        if y > maxy:
            maxy = y
        if y < miny:
            miny = y
    ax.append(ax[0])
    ay.append(ay[0])
    abscissas.append(ax)
    ordenadas.append(ay)

def le_entrada():
    global n_poly

    n_poly=int(sys.stdin.readline()) #
    for p in range(n_poly):
        le_poligono(p)
        
abscissas = []
ordenadas = []
maxx = -99999
maxy = -99999
minx = 99999
miny = 99999
le_entrada()
dx = maxx - minx
dy = maxy - miny
print (dx, dy)
print (abscissas)
print (ordenadas)
#dimensoes da figura num retangulo de 16 por 21
fig = figure(figsize=(dx,dy))

#gravar na imagem os poligonos
for p in range(n_poly):
    text(sum(abscissas[p]) / len(abscissas[p]), sum(ordenadas[p]) / len(ordenadas[p]), 'P{}'.format(p + 1), horizontalalignment='center', verticalalignment='center')
    fill(abscissas[p], ordenadas[p])

show()

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include "aligned-memory.h"
#include "const.h"
#include "polygon.h"

typedef struct Options Options;

#define min(a, b) ((a) <= (b) ? (a) : (b))
#define max(a, b) ((a) >= (b) ? (a) : (b))

struct Options {
    FILE *input;
    FILE *output;
    enum PolygonPointOrder polygonPointOrder;
};

static Options *getOptions(int argc, char *argv[]) {
    Options *opt = (Options *)malloc(sizeof(Options));
    opt->input = stdin;
    opt->output = stdout;
    opt->polygonPointOrder = PolygonPointOrder_CounterClockWise;

    char option;
    while ((option = getopt(argc, argv, "i:o:c")) > 0) {
        switch (option) {
            case 'i':
                opt->input = fopen(optarg, "r");
            break;
            case 'o':
                opt->output = fopen(optarg, "w");
            break;
            case 'c':
                opt->polygonPointOrder = PolygonPointOrder_ClockWise;
            break;
        }
    }

    return opt;
}

int main(int argc, char *argv[]) {
    Options *options = getOptions(argc, argv);

    int numPolygons;
    assert(fscanf(options->input, "%d", &numPolygons) == 1);

    PolygonSet *set = PolygonSet_new();

    for (int i = 0; i < numPolygons; i++) {
        Polygon *polygon = Polygon_readFile(options->input, options->polygonPointOrder);
        PolygonSet_addPolygon(set, polygon);
    }

    IntersectionSet *intersections = PolygonSet_getIntersections(set);

    char *intersectionMatrix = (char *)alignedMalloc(ALIGNMENT, (set->polygonsLength << 1) * sizeof(char));
    memset(intersectionMatrix, 0, (set->polygonsLength << 1) * sizeof(char));

    for (int i = 0; i < intersections->intersectionsLength; i++) {
        Intersection *intersection = intersections->intersections[i];

        if (
            intersection->edgeA->polygon != intersection->edgeB->polygon &&
            Polygon_isConvex(intersection->edgeA->polygon) &&
            Polygon_isConvex(intersection->edgeB->polygon)
        ) {
            int minId = min(intersection->edgeA->polygon->id, intersection->edgeB->polygon->id);
            int maxId = max(intersection->edgeA->polygon->id, intersection->edgeB->polygon->id);

            intersectionMatrix[set->polygonsLength * (minId - 1) + (maxId - 1)] = 1;
        }
    }

    for (int i = 0; i < set->polygonsLength; i++) {
        Polygon *pa = set->polygons[i];
        for (int j = i + 1; j < set->polygonsLength; j++) {
            Polygon *pb = set->polygons[j];

            int minId = min(pa->id, pb->id);
            int maxId = max(pa->id, pb->id);

            if (intersectionMatrix[set->polygonsLength * (minId - 1) + (maxId - 1)] == 1) {
                printf("(%d,%d)\n", minId, maxId);
            }
        }
    }

    for (int i = 0; i < set->polygonsLength; i++) {
        Polygon *p = set->polygons[i];
        const char *type = 
            !Polygon_isSimple(p) ? "nao simples" :
            Polygon_isConvex(p) ? "simples e convexo" :
            "simples e nao convexo";
        printf("%d %s\n", p->id, type);
    }

    IntersectionSet_freeIntersections(intersections);
    IntersectionSet_free(intersections);
    PolygonSet_freePolygons(set);
    PolygonSet_free(set);
    return 0;
}
#include <stdlib.h>
#include "aligned-memory.h"
#include "const.h"
#include "list.h"

static void CircularList_unlink(CircularList *list, CircularListItem *item);

static CircularListItem *CircularListItem_new(void *data);
static void CircularListItem_free(CircularListItem *item);

CircularList *CircularList_new() {
    CircularList *l = (CircularList *)alignedMalloc(ALIGNMENT, sizeof(CircularList));

    l->head = NULL;
    l->size = 0;

    return l;
}

void CircularList_free(CircularList *l) {
    while (l->head) {
        CircularList_pop(l);
    }
}

void *CircularList_pop(CircularList *list) {
    void *data = list->head->previous->data;
    CircularListItem *toDelete = list->head->previous;

    if (toDelete == list->head) {
        list->head = NULL;
    } else {
        list->head->previous = list->head->previous->previous;
    }

    CircularListItem_free(toDelete);

    list->size--;

    return data;
}

void *CircularList_tryRemove(CircularList *list, void *target) {
    if (list->size == 0) return NULL;
    CircularListItem *toUnlink = NULL;
    void *data = NULL;

    if (list->head->data == target) {
        toUnlink = list->head;
        data = toUnlink->data;

        if (list->size == 1) {
            list->head = NULL;
        } else {
            list->head = list->head->next;
        }
    } else {
        CircularListItem *runner = list->head->next;

        while (runner != list->head) {
            if (runner->data == target) {
                toUnlink = runner;
                data = toUnlink->data;
                break;
            }
            runner = runner->next;
        }
    }

    if (toUnlink != NULL) {
        CircularList_unlink(list, toUnlink);
        CircularListItem_free(toUnlink);
        list->size--;
    }

    return data;
}

void CircularList_push(CircularList *list, void *data) {
    CircularListItem *new = CircularListItem_new(data);

    if (list->head == NULL) {
        list->head = new;
        list->head->previous = list->head;
        list->head->next = list->head;
    } else {
        new->next = list->head;
        new->previous = list->head->previous;
        list->head->previous->next = new;
        list->head->previous = new;
    }

    list->size++;
}

void CircularList_forEach(CircularList *list, void (*consume)(void *data)) {
    if (list->size == 0) return;

    consume(list->head->data);

    CircularListItem *runner = list->head->next;
    while (runner != list->head) {
        consume(runner->data);
        runner = runner->next;
    }
}

static void CircularList_unlink(CircularList *list, CircularListItem *item) {
    if (list->head == item) {
        if (list->size == 1) {
            list->head = NULL;
        } else {
            list->head = list->head->next;
        }
    }

    item->next->previous = item->previous;
    item->previous->next = item->next;
    item->next = NULL;
    item->previous = NULL;
}

static CircularListItem *CircularListItem_new(void *data) {
    CircularListItem *item = (CircularListItem *)alignedMalloc(ALIGNMENT, sizeof(CircularListItem));

    item->data = data;
    item->previous = NULL;
    item->next = NULL;

    return item;
}

static void CircularListItem_free(CircularListItem *item) {
    free(item);
}

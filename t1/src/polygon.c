#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "aligned-memory.h"
#include "const.h"
#include "double.h"
#include "list.h"
#include "polygon.h"

/* CONSTANTS / GLOBALS */
#define MIN_SIZE 64

const char *PolygonTypeStrings[] = {
    "PolygonType_Unknown",
    "PolygonType_SimpleConvex",
    "PolygonType_SimpleConcave",
    "PolygonType_NonSimple"
};

const char *PolygonPointOrderStrings[] = {
    "PolygonPointOrder_ClockWise",
    "PolygonPointOrder_CounterClockWise"
};

static int id = 1;

/* SIGNATURES */
static Intersection *Intersection_new(double x, double y, Edge *edgeA, Edge *edgeB);
static void Intersection_free(Intersection *i);

static Point **PolygonSet_getAllPoints(PolygonSet *set);
static int PolygonSet_qsortPointXComparer(const void *a, const void *b);
static void PolygonSet_testIntersections(
    IntersectionSet *intersectionSet,
    CircularList *edgeList,
    Edge *target
);

static void Polygon_addEdge(Polygon *p, Point *head, Point *tail);
static void Polygon_updateCrossProductInfo(Polygon *p, Point *a, Point *b, Point *c, Point *d);
static void Polygon_onPointAdded(Polygon *polygon, Point *p);

/* METHODS */
/* INTERSECTION SET */
IntersectionSet *IntersectionSet_new() {
    IntersectionSet *set = (IntersectionSet *)alignedMalloc(ALIGNMENT, sizeof(IntersectionSet));

    set->intersections = (Intersection **)alignedMalloc(ALIGNMENT, MIN_SIZE * sizeof(Intersection *));
    set->intersectionsLength = 0;
    set->intersectionsAllocated = MIN_SIZE;

    return set;
}

void IntersectionSet_freeIntersections(IntersectionSet *set) {
    for (int i = 0; i < set->intersectionsLength; i++) {
        Intersection_free(set->intersections[i]);
    }
}

void IntersectionSet_free(IntersectionSet *set) {
    free(set->intersections);
    free(set);
}

static void IntersectionSet_addIntersection(IntersectionSet *set, Intersection *i) {
    set->intersections[set->intersectionsLength++] = i;
    //IntersectionSet_onIntersectionAdded(set, i);

    _reallocIfNecessary(
        set->intersections,
        set->intersectionsLength,
        set->intersectionsAllocated
    );
}

/* INTERSECTION */
static Intersection *Intersection_new(double x, double y, Edge *edgeA, Edge *edgeB) {
    Intersection *i = (Intersection *)alignedMalloc(ALIGNMENT, sizeof(Intersection));

    i->x = x;
    i->y = y;
    i->edgeA = edgeA;
    i->edgeB = edgeB;

    return i;
}

static void Intersection_free(Intersection *i) {
    free(i);
}

/* EDGE */
static int Edge_isVertical(Edge *a) {
    return Double_isEqual(a->head->x, a->tail->x);
}

static int Edge_contains(Edge *a, Point *p) {
    if (
        (
            Double_isEqual(a->head->x, p->x) &&
            Double_isEqual(a->head->y, p->y)
        ) ||
        (
            Double_isEqual(a->tail->x, p->x) &&
            Double_isEqual(a->tail->y, p->y)
        )
    ) {
        return 0;
    }

    if (Edge_isVertical(a)) {
        return (
            p->x == a->bottomMost->x &&
            a->bottomMost->y < p->y && p->y < a->topMost->y
        );
    }

    return (
        // point is in line
        Double_isEqual(
            (p->y - a->y0),
            (a->slope * p->x)
        ) &&
        // point is inside the box
        (
            a->bottomMost->y <= p->y && p->y <= a->topMost->y &&
            a->leftMost->x <= p->x && p->x <= a->rightMost->x
        )
    );
}

static int Edge_doesXProjectionIntersects(Edge *a, Edge *b) {
    return (
        (a->leftMost->x <= b->leftMost->x && b->leftMost->x <= a->rightMost->x) ||
        (a->leftMost->x <= b->rightMost->x && b->rightMost->x <= a->rightMost->x) ||
        (b->leftMost->x <= a->leftMost->x && a->leftMost->x <= b->rightMost->x) ||
        (b->leftMost->x <= a->rightMost->x && a->rightMost->x <= b->rightMost->x)
    );
}

static int Edge_doesYProjectionIntersects(Edge *a, Edge *b) {
    return (
        (a->bottomMost->y <= b->bottomMost->y && b->bottomMost->y <= a->topMost->y) ||
        (a->bottomMost->y <= b->topMost->y && b->topMost->y <= a->topMost->y) ||
        (b->bottomMost->y <= a->bottomMost->y && a->bottomMost->y <= b->topMost->y) ||
        (b->bottomMost->y <= a->topMost->y && a->topMost->y <= b->topMost->y)
    );
}

static int Edge_doesXYProjectionsIntersect(Edge *a, Edge *b) {
    return (
        Edge_doesXProjectionIntersects(a, b) &&
        Edge_doesYProjectionIntersects(a, b)
    );
}

static Intersection *Edge_getIntersection(Edge *a, Edge *b) {
    if (!Edge_doesXYProjectionsIntersect(a, b)) {
        return NULL;
    }

    VERBOSE("Edge a (P%d): (%.20g,%.20g), (%.20g,%.20g)\n", a->polygon->id, a->head->x, a->head->y, a->tail->x, a->tail->y);
    VERBOSE("Edge b (P%d): (%.20g,%.20g), (%.20g,%.20g)\n", b->polygon->id, b->head->x, b->head->y, b->tail->x, b->tail->y);

    if (Edge_contains(a, b->head)) {
        VERBOSE("Edge a contains b->head\n");
        return Intersection_new(b->head->x, b->head->y, a, b);
    }
    if (Edge_contains(a, b->tail)) {
        VERBOSE("Edge a contains b->tail\n");
        return Intersection_new(b->tail->x, b->tail->y, a, b);
    }
    if (Edge_contains(b, a->head)) {
        VERBOSE("Edge b contains a->head\n");
        return Intersection_new(a->head->x, a->head->y, a, b);
    }
    if (Edge_contains(b, a->tail)) {
        VERBOSE("Edge b contains a->tail\n");
        return Intersection_new(a->tail->x, a->tail->y, a, b);
    }
    // if (Edge_isCollinear(a, b)) {
    //     VERBOSE("Edge a and b are collinear\n");
    //     VERBOSE("Intersection point is a common vertex\n");
    //     return NULL;
    // }

    double x = 0.0;
    double y = 0.0;

    if (Edge_isVertical(a)) {
        // b is not vertical
        VERBOSE("Edge a is vertical\n");
        x = a->head->x;
        y = b->slope * x - b->slope * b->head->x + b->head->y;
    } else if (Edge_isVertical(b)) {
        // a is not vertical
        VERBOSE("Edge b is vertical\n");
        x = b->head->x;
        y = a->slope * x - a->slope * a->head->x + a->head->y;
    } else {
        x = (b->y0 - a->y0) / (a->slope - b->slope);
        y = a->slope * x + a->y0;
    }

    VERBOSE("Edge a and b are not collinear\n");
    VERBOSE("Intersection point: (%.20g,%.20g)\n", x, y);

    Point p = (Point){ .x = x, .y = y };
    if (!Edge_contains(a, &p) && !Edge_contains(b, &p)) {
        VERBOSE("Intersection point is not within edges a and b\n");
        return NULL;
    }

    if (!Edge_contains(a, &p) && Edge_contains(b, &p)) {
        VERBOSE("Intersection point is not within edge a\n");
        return NULL;
    }

    if (Edge_contains(a, &p) && !Edge_contains(b, &p)) {
        VERBOSE("Intersection point is not within edge b\n");
        return NULL;
    }

    return Intersection_new(x, y, a, b);
}

static void Edge_init(Edge *e, Point *head, Point *tail, Polygon *polygon) {
    e->head = head;
    e->tail = tail;
    e->polygon = polygon;

    if (head->x <= tail->x) {
        e->leftMost = head;
        e->rightMost = tail;
    } else {
        e->leftMost = tail;
        e->rightMost = head;
    }

    if (head->y >= tail->y) {
        e->topMost = head;
        e->bottomMost = tail;
    } else {
        e->topMost = tail;
        e->bottomMost = head;
    }

    e->slope = (e->rightMost->y - e->leftMost->y) / (e->rightMost->x - e->leftMost->x);
    e->y0 = e->head->y - (e->slope * e->head->x);
}

/* POINT */
static void Point_init(Point *p, int index, double x, double y, Polygon *polygon) {
    p->index = index;
    p->x = x;
    p->y = y;
    p->polygon = polygon;
    p->head = NULL;
    p->tail = NULL;
}

/* POLYGON SET */
PolygonSet *PolygonSet_new() {
    PolygonSet *set = (PolygonSet *)alignedMalloc(ALIGNMENT, sizeof(PolygonSet));

    set->polygons = (Polygon **)alignedMalloc(ALIGNMENT, MIN_SIZE * sizeof(Polygon *));
    set->polygonsLength = 0;
    set->polygonsAllocated = MIN_SIZE;
    set->numberOfPoints = 0;
    set->numberOfEdges = 0;

    return set;
}

void PolygonSet_free(PolygonSet *set) {
    free(set->polygons);
    free(set);
}

void PolygonSet_freePolygons(PolygonSet *set) {
    for (int i = 0; i < set->polygonsLength; i++) {
        Polygon_free(set->polygons[i]);
    }
}

void PolygonSet_addPolygon(PolygonSet *set, Polygon *p) {
    set->polygons[set->polygonsLength++] = p;
    set->numberOfPoints += p->pointsLength;
    set->numberOfEdges += p->edgesLength;
    _reallocIfNecessary(
        set->polygons,
        set->polygonsLength,
        set->polygonsAllocated
    );
}

IntersectionSet *PolygonSet_getIntersections(PolygonSet *set) {
    IntersectionSet *intersectionSet = IntersectionSet_new();
    Point **points = PolygonSet_getAllPoints(set);
    qsort(
        points,
        set->numberOfPoints,
        sizeof(Point *),
        PolygonSet_qsortPointXComparer
    );

    CircularList *edgeList = CircularList_new();

    for (int i = 0; i < set->numberOfPoints; i++) {
        Point *p = points[i];

        // exits first
        if (p == p->head->rightMost) {
            CircularList_tryRemove(edgeList, p->head);
        }
        if (p == p->tail->rightMost) {
            CircularList_tryRemove(edgeList, p->tail);
        }

        if (p == p->head->leftMost) {
            // test p->head against all edges in the list
            PolygonSet_testIntersections(intersectionSet, edgeList, p->head);
            CircularList_push(edgeList, p->head);
        }
        if (p == p->tail->leftMost) {
            // test p->tail against all edges in the list
            PolygonSet_testIntersections(intersectionSet, edgeList, p->tail);
            CircularList_push(edgeList, p->tail);
        }
    }

    free(points);
    CircularList_free(edgeList);
    return intersectionSet;
}

static void PolygonSet_testIntersections(
    IntersectionSet *intersectionSet,
    CircularList *edgeList,
    Edge *target
) {
    if (edgeList->size == 0) return;

    Intersection *intersection = Edge_getIntersection(
        target,
        (Edge *)edgeList->head->data
    );

    if (intersection != NULL) {
        if (intersection->edgeA->polygon == intersection->edgeB->polygon) {
            intersection->edgeA->polygon->flags.hasSelfIntersection = 1;
        }
        IntersectionSet_addIntersection(intersectionSet, intersection);
    }

    CircularListItem *runner = edgeList->head->next;

    while (runner != edgeList->head) {
        intersection = Edge_getIntersection(target, (Edge *)runner->data);

        if (intersection != NULL) {
            if (intersection->edgeA->polygon == intersection->edgeB->polygon) {
                intersection->edgeA->polygon->flags.hasSelfIntersection = 1;
            }
            IntersectionSet_addIntersection(intersectionSet, intersection);
        }

        runner = runner->next;
    }
}

static int PolygonSet_qsortPointXComparer(const void *a, const void *b) {
    Point *pa = *((Point **)(a));
    Point *pb = *((Point **)(b));

    if (pa->x != pb->x) {
        return pa->x > pb->x ? 1 : -1;
    }

    return pa->index > pb->index ? 1 : -1;
}

static Point **PolygonSet_getAllPoints(PolygonSet *set) {
    Point **points = (Point **)alignedMalloc(ALIGNMENT, set->numberOfPoints * sizeof(Point *));
    for (int i = 0, k = 0; i < set->polygonsLength; i++) {
        Polygon *polygon = set->polygons[i];
        for (int j = 0; j < polygon->pointsLength; j++) {
            points[k++] = &polygon->points[j];
        }
    }
    return points;
}

/* POLYGON */
Polygon *Polygon_new(enum PolygonPointOrder pointOrder) {
    Polygon *p = (Polygon *)alignedMalloc(ALIGNMENT, sizeof(Polygon));

    p->id = id++;
    p->order = pointOrder;
    p->points = (Point *)alignedMalloc(ALIGNMENT, MIN_SIZE * sizeof(Point));
    p->pointsAllocated = MIN_SIZE;
    p->pointsLength = 0;
    p->edges = (Edge *)alignedMalloc(ALIGNMENT, MIN_SIZE * sizeof(Edge));
    p->edgesLength = 0;
    p->edgesAllocated = MIN_SIZE;

    p->flags.hasNegativeCrossProduct = 0;
    p->flags.hasPositiveCrossProduct = 0;
    p->flags.hasSelfIntersection = 0;
    p->flags.isClosed = 0;

    return p;
}

void Polygon_free(Polygon *p) {
    free(p->points);
    free(p->edges);
    free(p);
}

int Polygon_isSimple(Polygon *p) {
    return !p->flags.hasSelfIntersection;
}

int Polygon_isConvex(Polygon *p) {
    return (
        Polygon_isSimple(p) && (
            (p->flags.hasNegativeCrossProduct && !p->flags.hasPositiveCrossProduct) ||
            (!p->flags.hasNegativeCrossProduct && p->flags.hasPositiveCrossProduct)
        )
    );
}

// closes the polygon (i.e. connects the last point to the first)
// after this, you cannot add any more points to the polygon.
void Polygon_close(Polygon *p) {
    p->flags.isClosed = 1;

    Point *first = &p->points[0];
    Point *second = &p->points[1];
    Point *secondLast = &p->points[p->pointsLength - 2];
    Point *last = &p->points[p->pointsLength - 1];

    Polygon_addEdge(p, last, first);
    Polygon_updateCrossProductInfo(p, secondLast, last, last, first);
    Polygon_updateCrossProductInfo(p, last, first, first, second);
}

void Polygon_addPoint(Polygon *p, double x, double y) {
    Point *point = &p->points[p->pointsLength];

    Point_init(point, p->pointsLength, x, y, p);
    Polygon_onPointAdded(p, point);

    p->pointsLength++;
    _reallocIfNecessary(p->points, p->pointsLength, p->pointsAllocated);
}

Polygon *Polygon_readFile(FILE *input, enum PolygonPointOrder order) {
    int numPoints;
    assert(fscanf(input, "%d", &numPoints) == 1);

    Polygon *p = Polygon_new(order);

    double x, y;
    for (int i = 0; i < numPoints; i++) {
        assert(fscanf(input, "%lf %lf", &x, &y) == 2);
        Polygon_addPoint(p, x, y);
    }

    Polygon_close(p);

    return p;
}

void Polygon_print(FILE *output, Polygon *polygon) {
    VERBOSE("Printing polygon...\n");

    // fprintf(output, "%s\n", PolygonPointOrderStrings[polygon->order]);
    fprintf(output, "%d\n", polygon->pointsLength);

    for (int i = 0; i < polygon->pointsLength; i++) {
        Point *p = &polygon->points[i];
        fprintf(output, "%.20g %.20g\n", p->x, p->y);
    }
}

static void Polygon_onEdgeAdded(Edge *e) {
    e->head->head = e;
    e->tail->tail = e;
}

static void Polygon_addEdge(Polygon *polygon, Point *head, Point *tail) {
    Edge *e = &polygon->edges[polygon->edgesLength++];

    Edge_init(e, head, tail, polygon);
    Polygon_onEdgeAdded(e);

    _reallocIfNecessary(
        polygon->edges,
        polygon->edgesLength,
        polygon->edgesAllocated
    );
}

// calculates cross product of vectors ab and cd
// https://stackoverflow.com/questions/471962/how-do-i-efficiently-determine-if-a-polygon-is-convex-non-convex-or-complex/45372025#45372025
static double Polygon_getCrossProduct(Point *a, Point *b, Point *c, Point *d) {
    double abx = b->x - a->x;
    double aby = b->y - a->y;
    double cdx = d->x - c->x;
    double cdy = d->y - c->y;

    return (abx * cdy) - (aby * cdx);
}

static void Polygon_updateCrossProductInfo(Polygon *p, Point *a, Point *b, Point *c, Point *d) {
    double crossProduct = Polygon_getCrossProduct(a, b, c, d);
        
    if (crossProduct >= 0.0) {
        p->flags.hasPositiveCrossProduct = 1;
    } else {
        p->flags.hasNegativeCrossProduct = 1;
    }
}

static int Polygon_getOffsetIndex(Polygon *polygon, Point *p, int offset) {
    int index = p->index + offset;
    if (index < 0) {
        return polygon->pointsLength + index;
    }
    if (index >= polygon->pointsLength) {
        return index % polygon->pointsLength;
    }
    return index;
}

static Point *Polygon_getOffsetPoint(Polygon *polygon, Point *p, int offset) {
    return &polygon->points[Polygon_getOffsetIndex(polygon, p, offset)];
}

static void Polygon_onPointAdded(Polygon *polygon, Point *p) {
    VERBOSE("Added point (%.20g, %.20g)\n", p->x, p->y);

    if (p->index >= 1) {
        Point *left = Polygon_getOffsetPoint(polygon, p, -1);
        Polygon_addEdge(polygon, left, p);
    }

    // there must be at least two points behind the one added
    // in order to calculate the cross product
    if (p->index >= 2) {
        Point *middle = Polygon_getOffsetPoint(polygon, p, -1);
        Point *left = Polygon_getOffsetPoint(polygon, p, -2);

        Polygon_updateCrossProductInfo(polygon, left, middle, middle, p);
    }
}
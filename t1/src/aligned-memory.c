#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include "const.h"
#include "aligned-memory.h"

void *alignedMalloc(int alignment, size_t size) {
    return aligned_alloc(alignment, size);
}

void *alignedRealloc(void *ptr, size_t oldSize, size_t memberSize, int alignment, size_t newSize) {
    void *newPtr = aligned_alloc(alignment, newSize * sizeof(memberSize));
    memcpy(newPtr, ptr, oldSize * memberSize);
    free(ptr);
    return newPtr;
}

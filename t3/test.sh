#!/bin/bash

for i in 100 200 300 400 500 600 700 800 900 1000;
do
    python3 test_generator.py $i -500 500 __mock__/test_$i
    echo "Testing $i points..."
    ./delaunay < __mock__/test_$i > /dev/null
    if [ "$?" != "0" ]; then
        exit
    fi
done

#ifndef __FLOATING_POINT_ARITHMETIC__
#define __FLOATING_POINT_ARITHMETIC__

#include <limits>

namespace FPMath {
    const double INFINITY = std::numeric_limits<double>::infinity();
    double abs(double x);
    double min(double a, double b);
    double max(double a, double b);
    double sqr(double x);
    bool isEqual(double a, double b);
    int compare(double a, double b);
}

#endif

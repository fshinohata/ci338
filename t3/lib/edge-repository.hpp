#ifndef __PLANE_EDGE_REPOSITORY__
#define __PLANE_EDGE_REPOSITORY__

#include <sys/types.h>
#include "edge.hpp"

namespace Delaunay {
    class EdgeRepository {
        private:
            uint allocated;

            /**
             * @brief adds edge to repository.
             *        does not check for duplicates
             * 
             * @param e     edge to be added
             */
            void add(Edge *e);

            /**
             * @brief searches for the edge that contains both points in the repository.
             * 
             * @param a         one point of the edge
             * @param b         other point of the edge
             * @return Edge*    edge (a,b), or NULL otherwise
             */
            Edge *search(Point *a, Point *b);

        public:
            Edge **edges;
            uint length;

            EdgeRepository();
            ~EdgeRepository();

            /**
             * @brief searches for the edge that contains both points in the repository, and creates it if it does not exist.
             * 
             * @param a         one point of the edge
             * @param b         other point of the edge
             * @return Edge*    edge (a,b)
             */
            Edge *searchOrCreate(Point *a, Point *b);
    };

    extern EdgeRepository *edgeRepository;
};

#endif

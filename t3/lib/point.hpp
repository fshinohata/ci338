#ifndef __PLANE_POINT__
#define __PLANE_POINT__

namespace Delaunay {
    class Point {
        public:
            int id;
            double x;
            double y;

            bool isImaginary;

            Point(double _x, double _y, bool _isImaginary = false);

            bool isEqual(double _x, double _y);
            bool isEqual(Point *p);
    };

    namespace PointPosition {
        int get(Point *a, Point *b, Point *m);
        bool isToTheRight(Point *a, Point *b, Point *m);
        bool isToTheRight(int position);
        bool isToTheLeft(Point *a, Point *b, Point *m);
        bool isToTheLeft(int position);
        bool isCollinear(Point *a, Point *b, Point *m);
        bool isCollinear(int position);
    };
};

#endif
#ifndef __PLANE_TRIANGLE__
#define __PLANE_TRIANGLE__

#include "point.hpp"

namespace Delaunay {
    class Edge;
    class Triangle {
        private:
            void setClockwiseOrder(Point *a, Point *b, Point *c);
            void init(Point *a, Point *b, Point *c);

        public:
            static int getId(Triangle *t);
            static int getIndex(Triangle *t);

            int id;
            int index = -1;

            Point *pointA = NULL;
            Point *pointB = NULL;
            Point *pointC = NULL;

            Edge *edgeAB = NULL;
            Edge *edgeBC = NULL;
            Edge *edgeCA = NULL;

            bool isImaginary;
            bool isVisited;

            Triangle(Point *a, Point *b, Point *c);
            ~Triangle();

            Triangle *oppositeA();
            Triangle *oppositeB();
            Triangle *oppositeC();

            void clearOwnReferences();
            void resetPoints(Point *a, Point *b, Point *c);
            bool contains(Point *p);
            void updateOpposites(void);
            Edge *getCommonEdge(Triangle *t);
            Point *getDistinctPoint(Triangle *t);
            bool isInsideCircumference(Point *p);

            /**
             * @brief Returns the opposite triangle of vertex p if it is one of the triangle's vertices.
             * 
             * @param p             point p
             * @return Triangle*    opposite triangle from p. aborts if p is not one of the triangle's vertices.
             */
            Triangle *getOpposite(Point *p);
    };
};

#endif
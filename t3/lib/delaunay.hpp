#ifndef __DELAUNAY_TRIANGULATION__
#define __DELAUNAY_TRIANGULATION__

#include <vector>
#include "triangulation.hpp"
#include "point.hpp"

namespace Delaunay {
    Triangulation *getTriangulation_(std::vector<Point *> *points);
};

#endif

#ifndef __PLANE_EDGE__
#define __PLANE_EDGE__

#include "point.hpp"


namespace Delaunay {
    class Triangle;
    class Edge {
        public:
            int index = -1;
            Point *pointA = NULL;
            Point *pointB = NULL;

            Triangle *triangleA = NULL;
            Triangle *triangleB = NULL;

            bool isImaginary = false;

            Edge(Point *a, Point *b);

            bool isEqual(Point *a, Point *b);
            int compare(Point *a, Point *b);
            int compare(Edge *e);

            bool hasNeighbor(Triangle *t);
            void setTriangle(Triangle *t);
            void removeTriangle(Triangle *t);
            Triangle *otherTriangle(Triangle *t);
    };
};

#endif

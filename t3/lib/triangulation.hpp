#ifndef __PLANE_TRIANGULATION__
#define __PLANE_TRIANGULATION__

#include <vector>
#include "point.hpp"

namespace Delaunay {
    class Triangulation {
        private:
            std::vector<Triangle *> *triangles;
            unsigned int _size;
            void setAllUnvisited();

        public:
            // access "triangles" vector
            Triangle *operator [](int idx);

            // ctor/dtor
            Triangulation();
            ~Triangulation();

            // adds triangle t to vector.
            // does not check for duplicates.
            void add(Triangle *t);

            // removes triangle t from vector.
            // does not check if the triangle is indeed in the vector
            void remove(Triangle *t);

            // returns number of triangles of triangulation
            int size();

            /**
             * @brief searches for the triangle that contains point p.
             * 
             * @param p             point to search
             * @return Triangle*    triangle that contains p, or NULL otherwise
             */
            Triangle *searchTriangleWithPoint(Point *p);
    };
};

#endif

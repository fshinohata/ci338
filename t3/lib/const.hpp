#ifndef __CONSTANTS__
#define __CONSTANTS__

#include <stdio.h>
#include <stdlib.h>

#define CACHE_LINE_SIZE 64
#define ALIGNMENT CACHE_LINE_SIZE

#define AUDIT_PRINT(format, ...) { fprintf(stderr, "(%s) [%s:%d] " format, __TIME__, __FILE__, __LINE__, ##__VA_ARGS__); fflush(stderr); }

#ifdef _DEBUG_
    #define VERBOSE(format, ...) AUDIT_PRINT(format, ##__VA_ARGS__)
#else
    #define VERBOSE(...)
#endif

#define ABORT(code, format, ...) { AUDIT_PRINT("[ERROR CODE: %d] " format, code, ##__VA_ARGS__); exit(code); }

namespace ErrorCode {
    const int OK = 0;
    const int LESS_THAN_THREE_POINTS = 1;
    const int DEGENERATED_TRIANGLE = 2;
    const int IMAGINARY_TRIANGLE_NOT_LARGE_ENOUGH = 3;
    const int TOO_MANY_TRIANGLES_FOR_EDGE = 4;
    const int EDGE_DOES_NOT_HAVE_TRIANGLE = 5;
    const int EMPTY_TRIANGULATION = 6;
    const int POINT_NOT_IN_TRIANGULATION = 7;
    const int TRIANGLE_NOT_NEIGHBOR = 8;
    const int POINT_IS_NOT_A_VERTEX = 9;
    const int FINAL_TRIANGLE_IS_NOT_DELAUNAY = 10;
    const int EDGE_IS_NOT_PART_OF_TRIANGLE = 11;
    const int NOT_IMPLEMENTED = 99;
};

#endif
#!/usr/bin/python3
import sys
import numpy as np

def alreadyGenerated(pointsGenerated, point):
    for p in pointsGenerated:
        if p[0] == point[0] and p[1] == point[1]:
            return True
    return False

def main():
    if len(sys.argv) < 5:
        print("Usage: {} [n-points] [min] [max] [dest]".format(sys.argv[0]))
        exit(1)

    numPoints = int(sys.argv[1])
    _min = int(sys.argv[2])
    _max = int(sys.argv[3])
    dest = sys.argv[4]

    f = open(dest, mode="w")
    f.write("{}\n".format(numPoints))

    pointsGenerated = []
    numPointsGenerated = 0

    while numPointsGenerated < numPoints:
        x = np.random.randint(_min, _max)
        y = np.random.randint(_min, _max)

        if not alreadyGenerated(pointsGenerated, [x, y]):
            numPointsGenerated += 1
            pointsGenerated.append([x, y])
            f.write("{} {}\n".format(x, y))

if __name__ == "__main__":
    main()
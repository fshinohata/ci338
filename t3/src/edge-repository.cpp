#include <sys/types.h>
#include <stdlib.h>
#include "const.hpp"
#include "fp-math.hpp"
#include "edge-repository.hpp"

#define MIN_SIZE 1

namespace Delaunay {
    EdgeRepository *edgeRepository = new EdgeRepository();

    EdgeRepository::EdgeRepository() {
        edges = (Edge **)malloc(MIN_SIZE * sizeof(*edges));
        allocated = MIN_SIZE;
        length = 0;
    }

    EdgeRepository::~EdgeRepository() {
        for (uint i = 0; i < length; i++) {
            delete edges[i];
        }
        free(edges);
    }

    void EdgeRepository::add(Edge *e) {
        uint i = length;

        while (i > 0 && edges[i - 1]->compare(e) > 0) {
            edges[i] = edges[i - 1];
            edges[i]->index = i;
            i--;
        }

        edges[i] = e;
        e->index = i;

        length++;

        if (length == allocated) {
            allocated <<= 1;
            edges = (Edge **)realloc(edges, allocated * sizeof(*edges));
        }
    }

    Edge *EdgeRepository::search(Point *a, Point *b) {
        int min = 0;
        int max = length - 1;
        int middle = (min + max) / 2;

        while (min <= max) {
            // VERBOSE("Comparing edges [(%.20g,%.20g),(%.20g,%.20g)] and [(%.20g,%.20g),(%.20g,%.20g)]...\n",
            //     edges[middle]->pointA->x, edges[middle]->pointA->y,
            //     edges[middle]->pointB->x, edges[middle]->pointB->y,
            //     a->x, a->y,
            //     b->x, b->y
            // );
            int comparison = edges[middle]->compare(a, b);

            if (comparison == 0) {
                // VERBOSE("EQUALS\n");
                return edges[middle];
            } else if (comparison > 0) {
                // VERBOSE("GREATER THAN\n");
                max = middle - 1;
            } else {
                // VERBOSE("LESS THAN\n");
                min = middle + 1;
            }

            middle = (min + max) / 2;
        }

        return NULL;
    }

    Edge *EdgeRepository::searchOrCreate(Point *a, Point *b) {
        Edge *fromRepo = search(a, b);

        if (fromRepo != NULL) {
            return fromRepo;
        }

        VERBOSE("Creating new edge [(%.20g,%.20g),(%.20g,%.20g)]\n", a->x, a->y, b->x, b->y);

        Edge *newEdge = new Edge(a, b);
        add(newEdge);
        return newEdge;
    }
};

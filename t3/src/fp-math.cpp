#include "fp-math.hpp"

#define EPSILON 0.0000001

namespace FPMath {
    double abs(double x) {
        return x < 0 ? -x : x;
    }

    double min(double a, double b) {
        return a < b ? a : b;
    }

    double max(double a, double b) {
        return a > b ? a : b;
    }

    double sqr(double x) {
        return x * x;
    }

    bool isEqual(double a, double b) {
        return abs(a - b) <= EPSILON;
    }

    int compare(double a, double b) {
        if (isEqual(a, b)) return 0;
        return a < b ? -1 : 1;
    }
};

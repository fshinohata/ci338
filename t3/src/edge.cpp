#include "const.hpp"
#include "fp-math.hpp"
#include "point.hpp"
#include "edge.hpp"
#include "triangle.hpp"

namespace Delaunay {
    Edge::Edge(Point *a, Point *b) {
        pointA = a;
        pointB = b;
        isImaginary = a->isImaginary || b->isImaginary;
    }

    bool Edge::isEqual(Point *a, Point *b) {
        return (
            (pointA->isEqual(a) && pointB->isEqual(b)) ||
            (pointA->isEqual(b) && pointB->isEqual(a))
        );
    }

    int Edge::compare(Point *a, Point *b) {
        auto minX = FPMath::min(pointA->x, pointB->x);
        auto maxX = FPMath::max(pointA->x, pointB->x);
        auto minY = FPMath::min(pointA->y, pointB->y);
        auto maxY = FPMath::max(pointA->y, pointB->y);

        auto otherMinX = FPMath::min(a->x, b->x);
        auto otherMaxX = FPMath::max(a->x, b->x);
        auto otherMinY = FPMath::min(a->y, b->y);
        auto otherMaxY = FPMath::max(a->y, b->y);

        auto comparison = FPMath::compare(minX, otherMinX);

        if (comparison != 0) {
            return comparison;
        }

        comparison = FPMath::compare(maxX, otherMaxX);

        if (comparison != 0) {
            return comparison;
        }

        comparison = FPMath::compare(minY, otherMinY);

        if (comparison != 0) {
            return comparison;
        }

        return FPMath::compare(maxY, otherMaxY);
    }

    int Edge::compare(Edge *e) {
        return compare(e->pointA, e->pointB);
    }

    bool Edge::hasNeighbor(Triangle *t) {
        return (
            triangleA == t ||
            triangleB == t
        );
    }

    void Edge::setTriangle(Triangle *t) {
        if (triangleA == NULL) {
            triangleA = t;
        } else if (triangleB == NULL) {
            triangleB = t;
        } else {
            ABORT(ErrorCode::TOO_MANY_TRIANGLES_FOR_EDGE, "An edge cannot have more than two neighboring triangles.\n");
        }
    }

    void Edge::removeTriangle(Triangle *t) {
        if (triangleA == t) {
            triangleA = NULL;
        } else if (triangleB == t) {
            triangleB = NULL;
        } else {
            ABORT(ErrorCode::EDGE_DOES_NOT_HAVE_TRIANGLE, "The edge %d does not contain the triangle %d\n", index + 1, Triangle::getIndex(t));
        }
    }

    Triangle *Edge::otherTriangle(Triangle *t) {
        if (triangleA == t) {
            return triangleB;
        } else if (triangleB == t) {
            return triangleA;
        }

        ABORT(ErrorCode::EDGE_DOES_NOT_HAVE_TRIANGLE, "The edge %d does not contain the triangle %d\n", index + 1, Triangle::getIndex(t));
    }
};

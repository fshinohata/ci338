#include <vector>
#include "const.hpp"
#include "fp-math.hpp"
#include "triangle.hpp"
#include "edge-repository.hpp"
#include "triangulation.hpp"
#include "delaunay.hpp"

namespace Delaunay {
    /**
     * @brief Calculated first imaginary super triangle that contains all the points given.
     * 
     * @param points        points that the triangle must contain
     * @return Triangle*    calculated imaginary triangle
     */
    static Triangle *getFirstImaginaryTriangle_(std::vector<Point *> *points) {
        auto minX = FPMath::INFINITY;
        auto minY = FPMath::INFINITY;
        auto maxX = -FPMath::INFINITY;
        auto maxY = -FPMath::INFINITY;

        for (uint i = 0; i < points->size(); i++) {
            Point *p = (*points)[i];

            minX = FPMath::min(minX, p->x);
            minY = FPMath::min(minY, p->y);
            maxX = FPMath::max(maxX, p->x);
            maxY = FPMath::max(maxY, p->y);
        }

        minX -= (maxX - minX);
        minY -= (maxY - minY);
        maxX += (maxX - minX);
        maxY += (maxY - minY);

        auto max = FPMath::max(
            FPMath::max(
                FPMath::abs(minX),
                FPMath::abs(maxX)
            ),
            FPMath::max(
                FPMath::abs(minY),
                FPMath::abs(maxY)
            )
        );

        return new Triangle(
            /* a: */ new Point(-3 * max, -3 * max, /* _isImaginary: */ true),
            /* b: */ new Point(3 * max, -3 * max, /* _isImaginary: */ true),
            /* c: */ new Point(0, 3 * max, /* _isImaginary: */ true)
        );
    }

    /**
     * @brief Performs the flip operation
     * 
     * @param t1                triangle t1 to be updated
     * @param t2                triangle t2 to be updated
     * @param flippedEdge       calculated flipped edge
     * @param p1                third point of new t1
     * @param p2                third point of new t2
     */
    void flipEdges(Triangle *t1, Triangle *t2, Edge *flippedEdge, Point *p1, Point *p2) {
        t1->clearOwnReferences();
        t2->clearOwnReferences();
        t1->resetPoints(p1, flippedEdge->pointA, flippedEdge->pointB);
        t2->resetPoints(p2, flippedEdge->pointA, flippedEdge->pointB);
    }

    /**
     * @brief Flip edges of neighbor triangles t1 and t2 to maintain delaunay's property.
     *        Does not check if the triangles are really neighbors of one another.
     * 
     * @param t1 triangle t1
     * @param t2 triangle t2
     */
    void flipEdges(Triangle *t1, Triangle *t2) {
        Edge *commonEdge = t1->getCommonEdge(t2);

        VERBOSE("Flipping common edge of triangles %d and %d\n", t1->id, t2->id);

        if (commonEdge == t1->edgeAB) {
            if (commonEdge == t2->edgeAB) {
                VERBOSE("1 Flipped edge: [(%.20g,%.20g),(%.20g,%.20g)]\n", t1->pointC->x, t1->pointC->y, t2->pointC->x, t2->pointC->y);
                Edge *flippedEdge = edgeRepository->searchOrCreate(t1->pointC, t2->pointC);
                flipEdges(t1, t2, flippedEdge, t1->pointA, t2->pointA);
            } else if (commonEdge == t2->edgeBC) {
                VERBOSE("2 Flipped edge: [(%.20g,%.20g),(%.20g,%.20g)]\n", t1->pointC->x, t1->pointC->y, t2->pointA->x, t2->pointA->y);
                Edge *flippedEdge = edgeRepository->searchOrCreate(t1->pointC, t2->pointA);
                flipEdges(t1, t2, flippedEdge, t1->pointA, t2->pointB);
            } else if (commonEdge == t2->edgeCA) {
                VERBOSE("3 Flipped edge: [(%.20g,%.20g),(%.20g,%.20g)]\n", t1->pointC->x, t1->pointC->y, t2->pointB->x, t2->pointB->y);
                Edge *flippedEdge = edgeRepository->searchOrCreate(t1->pointC, t2->pointB);
                flipEdges(t1, t2, flippedEdge, t1->pointA, t2->pointC);
            }
        } else if (commonEdge == t1->edgeBC) {
            if (commonEdge == t2->edgeAB) {
                VERBOSE("4 Flipped edge: [(%.20g,%.20g),(%.20g,%.20g)]\n", t1->pointA->x, t1->pointA->y, t2->pointC->x, t2->pointC->y);
                Edge *flippedEdge = edgeRepository->searchOrCreate(t1->pointA, t2->pointC);
                flipEdges(t1, t2, flippedEdge, t1->pointB, t2->pointA);
            } else if (commonEdge == t2->edgeBC) {
                VERBOSE("5 Flipped edge: [(%.20g,%.20g),(%.20g,%.20g)]\n", t1->pointA->x, t1->pointA->y, t2->pointA->x, t2->pointA->y);
                Edge *flippedEdge = edgeRepository->searchOrCreate(t1->pointA, t2->pointA);
                flipEdges(t1, t2, flippedEdge, t1->pointB, t2->pointB);
            } else if (commonEdge == t2->edgeCA) {
                VERBOSE("6 Flipped edge: [(%.20g,%.20g),(%.20g,%.20g)]\n", t1->pointA->x, t1->pointA->y, t2->pointB->x, t2->pointB->y);
                Edge *flippedEdge = edgeRepository->searchOrCreate(t1->pointA, t2->pointB);
                flipEdges(t1, t2, flippedEdge, t1->pointB, t2->pointC);
            }
        } else if (commonEdge == t1->edgeCA) {
            if (commonEdge == t2->edgeAB) {
                VERBOSE("7 Flipped edge: [(%.20g,%.20g),(%.20g,%.20g)]\n", t1->pointB->x, t1->pointB->y, t2->pointC->x, t2->pointC->y);
                Edge *flippedEdge = edgeRepository->searchOrCreate(t1->pointB, t2->pointC);
                flipEdges(t1, t2, flippedEdge, t1->pointC, t2->pointA);
            } else if (commonEdge == t2->edgeBC) {
                VERBOSE("8 Flipped edge: [(%.20g,%.20g),(%.20g,%.20g)]\n", t1->pointB->x, t1->pointB->y, t2->pointA->x, t2->pointA->y);
                Edge *flippedEdge = edgeRepository->searchOrCreate(t1->pointB, t2->pointA);
                flipEdges(t1, t2, flippedEdge, t1->pointC, t2->pointB);
            } else if (commonEdge == t2->edgeCA) {
                VERBOSE("9 Flipped edge: [(%.20g,%.20g),(%.20g,%.20g)]\n", t1->pointB->x, t1->pointB->y, t2->pointB->x, t2->pointB->y);
                Edge *flippedEdge = edgeRepository->searchOrCreate(t1->pointB, t2->pointB);
                flipEdges(t1, t2, flippedEdge, t1->pointC, t2->pointC);
            }
        }
    }

    /**
     * @brief Flip edges (if necessary) of neighbor triangles t1 and t2 to maintain delaunay's property.
     *        Does not check if the triangles are really neighbors of one another.
     * 
     * @param t1 triangle t1
     * @param t2 triangle t2
     */
    void flipEdgesIfNecessary(Triangle *t1, Triangle *t2, Point *inserted) {
        if (t1 == NULL || t2 == NULL) return;

        if (t1->isInsideCircumference(t2->getDistinctPoint(t1)) || t2->isInsideCircumference(t1->getDistinctPoint(t2))) {
            flipEdges(t1, t2);
            flipEdgesIfNecessary(t1, t1->getOpposite(inserted), inserted);
            flipEdgesIfNecessary(t2, t2->getOpposite(inserted), inserted);
        }
    }

    /**
     * @brief Flip edges (if necessary) of neighbor triangles t1, t2 and t3 to maintain delaunay's property.
     *        Does not check if the triangles are really neighbors of one another.
     * 
     * @param t1 triangle t1
     * @param t2 triangle t2
     * @param t3 triangle t3
     */
    void flipEdgesIfNecessary(Triangle *t1, Triangle *t2, Triangle *t3, Point *inserted) {
        flipEdgesIfNecessary(t1, t1->getOpposite(inserted), inserted);
        flipEdgesIfNecessary(t2, t2->getOpposite(inserted), inserted);
        flipEdgesIfNecessary(t3, t3->getOpposite(inserted), inserted);
    }

    /**
     * @brief Get the triangle edge that is collinear to point p
     * 
     * @param t         triangle to check
     * @param p         point to check
     * @return Edge*    edge that is collinear to p, or NULL if none of them are.
     */
    Edge *getEdgeCollinearToPoint(Triangle *t, Point *p) {
        if (PointPosition::isCollinear(t->pointA, t->pointB, p)) {
            return t->edgeAB;
        }

        if (PointPosition::isCollinear(t->pointB, t->pointC, p)) {
            return t->edgeBC;
        }

        if (PointPosition::isCollinear(t->pointC, t->pointA, p)) {
            return t->edgeCA;
        }

        return NULL;
    }

    /**
     * @brief Splits triangle t into three other triangles with common vertex p.
     *        The function assumes that p is inside t.
     * 
     * @param triangulation     triangulation object to update
     * @param t                 target triangle to split
     * @param p                 common vertex to use
     */
    void splitThree(Triangulation *triangulation, Triangle *t, Point *p) {
        // regular case, where p is indeed inside the triangle,
        // and is not collinear to any of it's edges

        // breaks triangle t into three
        auto a = t->pointA;
        auto b = t->pointB;
        auto c = t->pointC;

        triangulation->remove(t);
        delete t;

        auto t1 = new Triangle(a, b, p);
        auto t2 = new Triangle(b, c, p);
        auto t3 = new Triangle(c, a, p);

        flipEdgesIfNecessary(t1, t2, t3, p);

        triangulation->add(t1);
        triangulation->add(t2);
        triangulation->add(t3);
    }

    /**
     * @brief Splits both triangles of edge e into two triangles each, resulting in four new triangles.
     *        Point P must be inside edge e
     *        Triangles ABC and ABD will be split into BCP, CAP, BDP and DAP.
     * 
     * @param triangulation     triangulation object to update
     * @param e                 edge that contains p
     * @param p                 target point for the split
     */
    void splitFour(Triangulation *triangulation, Edge *e, Point *p) {
        // if one of the triangle edges is collinear to p,
        // then we have a degenerate case
        // solution: break the edge into two, and split the triangles
        // respectively, resulting in four new triangles
        // in this case, flipping edges is not necessary

        // triangles to split
        auto t1 = e->triangleA;
        auto t2 = e->triangleB;

        // new edges
        auto e1 = edgeRepository->searchOrCreate(e->pointA, p);
        auto e2 = edgeRepository->searchOrCreate(e->pointB, p);

        triangulation->remove(t1);
        triangulation->remove(t2);

        auto p1 = t1->getDistinctPoint(t2);
        auto p2 = t2->getDistinctPoint(t1);

        delete t1;
        delete t2;

        // t1 splitted
        auto t11 = new Triangle(p1, e1->pointA, e1->pointB);
        auto t12 = new Triangle(p1, e2->pointA, e2->pointB);

        triangulation->add(t11);
        triangulation->add(t12);

        // t2 splitted
        auto t21 = new Triangle(p2, e1->pointA, e1->pointB);
        auto t22 = new Triangle(p2, e2->pointA, e2->pointB);

        triangulation->add(t21);
        triangulation->add(t22);

        flipEdgesIfNecessary(t11, t11->getOpposite(p), p);
        flipEdgesIfNecessary(t12, t12->getOpposite(p), p);
        flipEdgesIfNecessary(t21, t21->getOpposite(p), p);
        flipEdgesIfNecessary(t22, t22->getOpposite(p), p);
    }

    /**
     * @brief Main logic to build the triangulation.
     *        The triangulation must already have the first imaginary triangle.
     * 
     * @param triangulation     Triangulation object with imaginary triangle
     * @param points            points to include
     */
    void buildTriangulationFromImaginaryTriangle(Triangulation *triangulation, std::vector<Point *> *points) {
        VERBOSE("Building triangulation with %ld points\n", points->size());
        for (uint i = 0; i < points->size(); i++) {
            auto p = (*points)[i];
            auto t = triangulation->searchTriangleWithPoint(p);

            if (t == NULL) {
                ABORT(ErrorCode::POINT_NOT_IN_TRIANGULATION, "Triangulation does not contain point (%.20g,%.20g)\n", p->x, p->y);
            }

            auto e = getEdgeCollinearToPoint(t, p);

            if (e == NULL) {
                splitThree(triangulation, t, p);
            } else {
                splitFour(triangulation, e, p);
            }
        }
    }

    /**
     * @brief removes all triangles that have at least one imaginary point.
     * 
     * @param triangulation     target triangulation object
     */
    void removeImaginaryTriangles(Triangulation *triangulation) {
        VERBOSE("Removing imaginary triangles\n");

        for (auto i = 0; i < triangulation->size(); i++) {
            auto t = (*triangulation)[i];

            if (t->isImaginary) {
                triangulation->remove(t);
                delete t;
                i--;
            }
        }
    }

    #ifdef _DEBUG_
    /**
     * @brief Debug function. Validates that all points are inside the created imaginary triangle.
     * 
     * @param imaginary     calculated imaginary triangle
     * @param points        points that must be inside
     */
    static void validateAllPointAreInsideImaginaryTriangle(Triangle *imaginary, std::vector<Point *> *points) {
        VERBOSE("Validating imaginary triangle...\n");
        for (uint i = 0; i < points->size(); i++) {
            auto p = (*points)[i];
            if (!imaginary->contains(p)) {
                ABORT(
                    ErrorCode::IMAGINARY_TRIANGLE_NOT_LARGE_ENOUGH,
                    "The imaginary triangle [(%.20g,%.20g),(%.20g,%.20g),(%.20g,%.20g)) does not contain the point (%.20g,%.20g)\n",
                    imaginary->pointA->x, imaginary->pointA->y,
                    imaginary->pointB->x, imaginary->pointB->y,
                    imaginary->pointC->x, imaginary->pointC->y,
                    p->x, p->y
                );
            }
        }
    }

    /**
     * @brief checks if all triangles of the triangulation are Delaunay. This takes quadratic time.
     * 
     * @param triangulation     target triangulation
     */
    void validateTriangulationIsDelaunay(Triangulation *triangulation) {
        VERBOSE("Validating that triangulation is delaunay...\n");
        for (auto i = 0; i < triangulation->size(); i++) {
            auto t = (*triangulation)[i];

            if (t->oppositeA()) {
                if (t->isInsideCircumference(t->oppositeA()->getDistinctPoint(t))) {
                    ABORT(ErrorCode::FINAL_TRIANGLE_IS_NOT_DELAUNAY, "Triangle %d is not delaunay: it contains triangle %d\n", t->index + 1, t->oppositeA()->index + 1);
                }
            }

            if (t->oppositeB()) {
                if (t->isInsideCircumference(t->oppositeB()->getDistinctPoint(t))) {
                    ABORT(ErrorCode::FINAL_TRIANGLE_IS_NOT_DELAUNAY, "Triangle %d is not delaunay: it contains triangle %d\n", t->index + 1, t->oppositeB()->index + 1);
                }
            }

            if (t->oppositeC()) {
                if (t->isInsideCircumference(t->oppositeC()->getDistinctPoint(t))) {
                    ABORT(ErrorCode::FINAL_TRIANGLE_IS_NOT_DELAUNAY, "Triangle %d is not delaunay: it contains triangle %d\n", t->index + 1, t->oppositeC()->index + 1);
                }
            }
        }
    }
    #endif

    /**
     * @brief Calculates a returns a Delaunay Triangulation of the points given.
     * 
     * @param points                points to use for the triangulation
     * @return Triangulation*       triangulation object
     */
    Triangulation *getTriangulation_(std::vector<Point *> *points) {
        if (points->size() < 3) {
            ABORT(ErrorCode::LESS_THAN_THREE_POINTS, "You must provide at least three points to create a triangulation.\n");
        }

        auto triangulation = new Triangulation();
        auto imaginary = getFirstImaginaryTriangle_(points);
        triangulation->add(imaginary);

        // save pointers to imaginary pointers to free then in the end
        auto imaginaryA = imaginary->pointA;
        auto imaginaryB = imaginary->pointB;
        auto imaginaryC = imaginary->pointC;

        #ifdef _DEBUG_
        validateAllPointAreInsideImaginaryTriangle(imaginary, points);
        #endif

        buildTriangulationFromImaginaryTriangle(triangulation, points);

        removeImaginaryTriangles(triangulation);

        #ifdef _DEBUG_
        validateTriangulationIsDelaunay(triangulation);
        #endif

        delete imaginaryA;
        delete imaginaryB;
        delete imaginaryC;

        return triangulation;
    }
}


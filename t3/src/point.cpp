#include "fp-math.hpp"
#include "point.hpp"

static int _id = 1;

namespace Delaunay {
    Point::Point(double _x, double _y, bool _isImaginary) {
        id = _id++;
        x = _x;
        y = _y;
        isImaginary = _isImaginary;
    }

    bool Point::isEqual(double _x, double _y) {
        return FPMath::isEqual(x, _x) && FPMath::isEqual(y, _y);
    }

    bool Point::isEqual(Point *p) {
        return isEqual(p->x, p->y);
    }

    namespace PointPosition {
        // calculates position of m in relation to vector ab
        // values:
        // 0: m is collinear to ab
        // 1: m is to the left of ab
        // -1: m is to the right os ab
        int get(Point *a, Point *b, Point *m) {
            // cross product of ab and am
            auto crossProduct = (b->x - a->x) * (m->y - a->y) - (b->y - a->y) * (m->x - a->x);

            if (FPMath::isEqual(crossProduct, 0.0)) {
                return 0;
            }

            return crossProduct > 0 ? 1 : -1;
        }

        bool isToTheRight(Point *a, Point *b, Point *m) {
            return isToTheRight(get(a, b, m));
        }

        bool isToTheRight(int position) {
            return position < 0;
        }

        bool isToTheLeft(Point *a, Point *b, Point *m) {
            return isToTheLeft(get(a, b, m));
        }

        bool isToTheLeft(int position) {
            return position > 0;
        }

        bool isCollinear(Point *a, Point *b, Point *m) {
            return isCollinear(get(a, b, m));
        }

        bool isCollinear(int position) {
            return position == 0;
        }
    }
};

#include "const.hpp"
#include "fp-math.hpp"
#include "triangle.hpp"
#include "edge-repository.hpp"

namespace Delaunay {
    static int _id = 1;

    Triangle::Triangle(Point *a, Point *b, Point *c) {
        id = _id++;

        init(a, b, c);
    }

    Triangle::~Triangle() {
        clearOwnReferences();
    }

    int Triangle::getId(Triangle *t) {
        return t == NULL ? 0 : t->id;
    }

    int Triangle::getIndex(Triangle *t) {
        return t == NULL ? 0 : t->index + 1;
    }

    void Triangle::setClockwiseOrder(Point *a, Point *b, Point *c) {
        auto center = Point(
            (a->x + b->x + c->x) / 3,
            (a->y + b->y + c->y) / 3
        );

        auto position = PointPosition::get(a, b, &center);

        if (PointPosition::isCollinear(position)) {
            ABORT(ErrorCode::DEGENERATED_TRIANGLE, "Received degenerated triangle [(%.20g,%.20g),(%.20g,%.20g),(%.20g,%.20g)].\n", a->x, a->y, b->x, b->y, c->x, c->y);
        }

        if (PointPosition::isToTheLeft(position)) {
            pointA = a;
            pointB = c;
            pointC = b;
        } else {
            pointA = a;
            pointB = b;
            pointC = c;
        }
    }

    void Triangle::clearOwnReferences() {
        if (edgeAB) {
            edgeAB->removeTriangle(this);
        }

        if (edgeBC) {
            edgeBC->removeTriangle(this);
        }

        if (edgeCA) {
            edgeCA->removeTriangle(this);
        }
    }

    void Triangle::init(Point *a, Point *b, Point *c) {
        setClockwiseOrder(a, b, c);

        isImaginary = a->isImaginary || b->isImaginary || c->isImaginary;

        edgeAB = edgeRepository->searchOrCreate(pointA, pointB);
        edgeBC = edgeRepository->searchOrCreate(pointB, pointC);
        edgeCA = edgeRepository->searchOrCreate(pointC, pointA);

        edgeAB->setTriangle(this);
        edgeBC->setTriangle(this);
        edgeCA->setTriangle(this);
    }

    void Triangle::resetPoints(Point *a, Point *b, Point *c) {
        init(a, b, c);
    }

    Triangle *Triangle::oppositeA() {
        return edgeBC->otherTriangle(this);
    }

    Triangle *Triangle::oppositeB() {
        return edgeCA->otherTriangle(this);
    }

    Triangle *Triangle::oppositeC() {
        return edgeAB->otherTriangle(this);
    }

    bool Triangle::contains(Point *p) {
        return (
            (PointPosition::isToTheRight(pointA, pointB, p) || PointPosition::isCollinear(pointA, pointB, p)) &&
            (PointPosition::isToTheRight(pointB, pointC, p) || PointPosition::isCollinear(pointB, pointC, p)) &&
            (PointPosition::isToTheRight(pointC, pointA, p) || PointPosition::isCollinear(pointC, pointA, p))
        );
    }

    Edge *Triangle::getCommonEdge(Triangle *t) {
        if (edgeAB->hasNeighbor(t)) {
            return edgeAB;
        }

        if (edgeBC->hasNeighbor(t)) {
            return edgeBC;
        }

        if (edgeCA->hasNeighbor(t)) {
            return edgeCA;
        }

        ABORT(ErrorCode::TRIANGLE_NOT_NEIGHBOR, "The triangle %d is not neighbor of triangle %d\n", id, t->id);
    }

    Point *Triangle::getDistinctPoint(Triangle *t) {
        if (edgeAB->hasNeighbor(t)) {
            return pointC;
        }

        if (edgeBC->hasNeighbor(t)) {
            return pointA;
        }

        if (edgeCA->hasNeighbor(t)) {
            return pointB;
        }

        ABORT(ErrorCode::TRIANGLE_NOT_NEIGHBOR, "The triangle %d is not neighbor of triangle %d\n", id, t->id);
    }

    bool Triangle::isInsideCircumference(Point *p) {
        double m[3][3] = {
            pointA->x - p->x, pointA->y - p->y, FPMath::sqr(pointA->x - p->x) + FPMath::sqr(pointA->y - p->y),
            pointB->x - p->x, pointB->y - p->y, FPMath::sqr(pointB->x - p->x) + FPMath::sqr(pointB->y - p->y),
            pointC->x - p->x, pointC->y - p->y, FPMath::sqr(pointC->x - p->x) + FPMath::sqr(pointC->y - p->y)
        };

        auto determinant = (
            (m[0][0] * m[1][1] * m[2][2]) +
            (m[0][1] * m[1][2] * m[2][0]) +
            (m[0][2] * m[1][0] * m[2][1]) -
            (m[2][0] * m[1][1] * m[0][2]) -
            (m[2][1] * m[1][2] * m[0][0]) -
            (m[2][2] * m[1][0] * m[0][1])
        );

        VERBOSE("Determinant result: %.20g\n", determinant);

        return FPMath::compare(determinant, 0.0) < 0;
    }

    Triangle *Triangle::getOpposite(Point *p) {
        if (p == pointA) {
            return oppositeA();
        } else if (p == pointB) {
            return oppositeB();
        } else if (p == pointC) {
            return oppositeC();
        }

        ABORT(ErrorCode::POINT_IS_NOT_A_VERTEX, "The point %d is not a point of triangle %d\n", p->id, id);
    }
};

#include <stdio.h>
#include <assert.h>
#include <vector>
#include "const.hpp"
#include "point.hpp"
#include "edge-repository.hpp"
#include "triangle.hpp"
#include "delaunay.hpp"

int main(void) {
    int numPoints;

    assert(1 == scanf("%d", &numPoints));
    printf("%d\n", numPoints);

    auto v = new std::vector<Delaunay::Point *>(numPoints);

    for (int i = 0; i < numPoints; i++) {
        double x, y;
        assert(2 == scanf("%lf %lf", &x, &y));

        auto p = new Delaunay::Point(x, y);
        (*v)[i] = p;

        printf("%.20g %.20g\n", x, y);
    }

    auto triangulation = Delaunay::getTriangulation_(v);

    VERBOSE("Final triangulation:\n");
    printf("%d\n", triangulation->size());

    for (int i = 0; i < triangulation->size(); i++) {
        Delaunay::Triangle *t = (*triangulation)[i];
        printf("%d %d %d %d %d %d\n", t->pointA->id, t->pointB->id, t->pointC->id, Delaunay::Triangle::getIndex(t->oppositeA()), Delaunay::Triangle::getIndex(t->oppositeB()), Delaunay::Triangle::getIndex(t->oppositeC()));
    }

    for (int i = 0; i < numPoints; i++) {
        delete (*v)[i];
    }

    delete v;
    delete triangulation;
    delete Delaunay::edgeRepository;

    return 0;
}
#include "const.hpp"
#include "edge.hpp"
#include "triangle.hpp"
#include "triangulation.hpp"

#define MIN_SIZE 64

namespace Delaunay {
    Triangulation::Triangulation() {
        triangles = new std::vector<Triangle *>(MIN_SIZE);
        _size = 0;
    }

    Triangulation::~Triangulation() {
        for (uint i = 0; i < _size; i++) {
            delete (*triangles)[i];
        }
        delete triangles;
    }

    Triangle *Triangulation::operator[](int idx) {
        return (*triangles)[idx];
    }

    void Triangulation::add(Triangle *t) {
        auto index = _size;
        (*triangles)[_size++] = t;
        t->index = index;

        if (_size == triangles->capacity()) {
            triangles->resize(triangles->capacity() << 1);
        }
    }

    void Triangulation::remove(Triangle *t) {
        int index = t->index;

        for (uint i = index; i < _size - 1; i++) {
            (*triangles)[i] = (*triangles)[i + 1];
            (*triangles)[i]->index = i;
        }

        _size--;
    }

    int Triangulation::size() {
        return _size;
    }

    void Triangulation::setAllUnvisited() {
        for (uint i = 0; i < _size; i++) {
            (*triangles)[i]->isVisited = false;
        }
    }

    Triangle *Triangulation::searchTriangleWithPoint(Point *p) {
        if (_size == 0) {
            ABORT(ErrorCode::EMPTY_TRIANGULATION, "Triangulation does not have any triangles\n");
        }

        VERBOSE("Searching for (%.20g,%.20g)...\n", p->x, p->y);

        setAllUnvisited();

        Triangle *t = (*triangles)[0];

        while (t != NULL) {
            VERBOSE("Checking triangle %d\n", t->id);
            t->isVisited = true;

            if (t->contains(p)) {
                VERBOSE("Point is inside triangle %d\n", t->id);
                return t;
            }

            Triangle *nextTriangle = NULL;

            if (PointPosition::isToTheLeft(t->pointA, t->pointB, p)) {
                nextTriangle = t->oppositeC();
            } else if (PointPosition::isToTheLeft(t->pointB, t->pointC, p)) {
                nextTriangle = t->oppositeA();
            } else if (PointPosition::isToTheLeft(t->pointC, t->pointA, p)) {
                nextTriangle = t->oppositeB();
            }

            if (nextTriangle == NULL || !nextTriangle->isVisited) {
                t = nextTriangle;
            }
        }

        return NULL;
    }
};